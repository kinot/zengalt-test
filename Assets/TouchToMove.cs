﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class TouchToMove : MonoBehaviour
{
    private bool firstClick = false;
    private Vector3 finishPoint;
    private Vector3 Point;
    private Queue arrPoints = new Queue();
    private float yAx;

    public Slider speedSlider;
    public float speed = 30;

    void Start()
    {
        yAx = gameObject.transform.position.y;
    }

    void Update()
    {
        

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray;

            ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Cube")
                {
                    firstClick = true;
                    finishPoint = hit.point;
                    arrPoints.Enqueue(finishPoint);
                    finishPoint.y = yAx;
                }
            }

        }

        if (arrPoints.Count > 0)
        {
            Point = (Vector3)arrPoints.Peek();
        }

        speed = speedSlider.value;
        if (firstClick && !Mathf.Approximately(gameObject.transform.position.magnitude, Point.magnitude))
        { 
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, Point, 1 / (speed * (Vector3.Distance(gameObject.transform.position, Point))));
        }
        else if (firstClick && Mathf.Approximately(gameObject.transform.position.magnitude, Point.magnitude))
        {
            if (arrPoints.Count > 0)
            {
                arrPoints.Dequeue();
            }
        }
    }
}